for


let isDebugActive = false

// console.log ()

let objectArray = []


class Cell {
    constructor(){
        this.carName = "brand";
    }
}

objectArray.push(new Cell())

console.log(objectArray)
console.log(objectArray[0].carName)

function includeFunction() {
    let includeArray = [false, false, false]
    let isIncludeTrue = includeArray.includes(true)
    console.log(isIncludeTrue)
}

function testWithObjectThis() {

    const cellObject = {
        name: null,
        arraySome: [0,1,2],
        lastArray: function() {
            outName(this.name)
        },
    }

    let catVasya = cellObject
    catVasya.name = "Vasya"
    let catTolya = cellObject
    catTolya.name = "Tolya"

    catTolya.lastArray()

    // catVasya.sayName
    // catTolya.sayName
}

function outName(name){
    console.log(name)
}

function testMinus () {
    let i = 4
    let j = -1
    let iEnd = 7
    let jEnd = 2
    debug ("isFreeAround" , null, `cell i${iEnd} cellj${jEnd}`)
    

    debug ("isFreeAround" , null, "I'm here")
    for (; j < jEnd; j++) {
        debug ("isFreeAround" , null, `i${i}j${j}`)
        for (; i < iEnd; i++) {
            debug ("isFreeAround" , null, `i${i}j${j}`)

            if (i>0&&j>0) {
                 debug ("iisFreeAround" , null, "I'm inside")

            }
            debug ("isFreeAround" , null, `iEnd${iEnd} jEnd${jEnd}`)

        }
    }

}

function array2d() {
    let array = [[1,2],[3,4]]
    let array2 = []
    array2.push([])
    array2.push([])
    array2.push([])
    array2.push([])
    array2.push([])
    array2[0][0] = 0
    array2[0].push(2)
    console.log(array2)
}

function sleep(ms) {
    return new Promise(
      resolve => setTimeout(resolve, ms)
    );
}

async function some() {   
let timeRange = 500

    while (true) {
        console.log("Hello");
        await sleep(timeRange);
        console.log("Tanya");
        await sleep(timeRange);
    }
}

function testSplice () {
    let testArray = [1,2,3,4]
    console.log(testArray)
    testArray.splice (3,1)
    console.log(testArray)


}

function testArray() {

    const array = [2, 5, 9];
    
    console.log(array);
    
    const index = array.indexOf(5);
    if (index > -1) {
      array.splice(index, 1); // 2nd parameter means remove one item only
    }
    
    // array = [2, 9]
    console.log(array); 
}

//DEBUG

function debug (debugType ,debugObject, debugText) {
    if (isDebugActive) {
        if (debugText === undefined){
            console.log(debugObject)
        } else if(debugObject===null) {
            console.log(debugText)
        }
         else {
            console.log(debugObject + debugText)
        }
    }
}


/*
let array = [1,2]

array.push(3)

let JointString = array.join("-")

console.log(JointString)
console.log(typeof JointString)
console.log('------------')
*/
/*
let someText = "@"
let email = "Email@gmail.com"

if(someText.test(email)){
    console.log("Содержит")
}
else{
    console.log("Не содержит")

}
*/
/*
function validateEmail(email) 
    {
        let ret = /\S+@\S+\.\S+/;
        return ret.test(email);
    }
    
console.log(validateEmail('anystring@anystring.anystring'));
*/

// console.log('Test');
